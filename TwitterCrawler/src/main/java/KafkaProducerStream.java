import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.Timer;
import java.awt.event.ActionListener;

import kafka.admin.AdminUtils;

import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import twitter4j.*;
import twitter4j.conf.*;
import twitter4j.json.DataObjectFactory;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import kafka.producer.KeyedMessage;


public class KafkaProducerStream {

    private TwitterStream twitterStream;
    private String topicName ;
    private String[] keyWords ;
    private String consumerKey ;
    private String consumerSecret ;
    private String accessToken ;
    private String accessTokenSecret ;


    public void initialize(){
        String consumerKey;
        String consumerSecret ;
        String accessToken ;
        String accessTokenSecret ;
        String topicName;
        String[] keyWords ;
        String keyLine;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Consumer Key");
        consumerKey=sc.nextLine();
        System.out.println("Enter Consumer Secret");
        consumerSecret=sc.nextLine();
        System.out.println("Enter Access Token");
        accessToken=sc.nextLine();
        System.out.println("Enter Access Secret");
        accessTokenSecret=sc.nextLine();
        System.out.println("Enter Kafka Topic to output");
        topicName=sc.nextLine();
        System.out.println("Enter key words (split with ',' ");
        keyLine=sc.nextLine();
        keyWords=keyLine.split(",");

        this.consumerKey=consumerKey;
        this.consumerSecret=consumerSecret;
        this.accessToken=accessToken;
        this.accessTokenSecret=accessTokenSecret;
        this.topicName=topicName;
        this.keyWords=keyWords;

    }

    public String getTopic(){
        return topicName;
    }

    public void cheat(String consumerKey, String consumerSecret, String accessToken , String accessTokenSecret, String topicName, String keyLine){
        keyWords=keyLine.split(",");


        this.consumerKey=consumerKey;
        this.consumerSecret=consumerSecret;
        this.accessToken=accessToken;
        this.accessTokenSecret=accessTokenSecret;
        this.topicName=topicName;


    }

    public void setup() {
        LinkedBlockingQueue<Status> queue = new LinkedBlockingQueue<Status>(1000);



        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accessToken)
                .setOAuthAccessTokenSecret(accessTokenSecret)
                .setJSONStoreEnabled(true);

        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

    }

    public void run(){

        StatusListener listener = new StatusListener() {




            @Override
            public void onStatus(Status status) {
                //  queue.offer(status);

                JSONObject jsonInp = new JSONObject(status);
                JSONObject jsonOut = new JSONObject();
                JSONObject jsonInpUser;


                try {

                    jsonInpUser= jsonInp.getJSONObject("user");
                    jsonOut
                            .put("tweetId", jsonInp.get("id"))
                            .put("retweet",jsonInp.get("retweet"))
                            .put("retweetCount",jsonInp.get("retweetCount"))
                            .put("favoriteCount",jsonInp.get("favoriteCount"))
                            .put("inReplyToStatusId",jsonInp.get("inReplyToStatusId"))
                            .put("retweeted",jsonInp.get("retweeted"))
                            .put("personId",jsonInpUser.get("id"));

                    if(jsonInp.has("place")){
                        JSONObject jsonInpPlace=jsonInp.getJSONObject("place");
                        jsonOut.put("PlaceName",jsonInpPlace.get("name"));
                    }
                    else
                        jsonOut.put("PlaceName","unknown");


                    //Getting the source in more meaningful way
                    String[] rawSource=jsonInp.get("source").toString().split(">");
                    String[] fullSource=rawSource[1].split("<");
                    jsonOut.put("source",fullSource[0]);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                KafkaWrapper.send(topicName,jsonOut.toString());

            }



            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                // System.out.println("Got a status deletion notice id:"
                //  statusDeletionNotice.getStatusId);
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                // System.out.println("Got track limitation notice:" +  numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                // System.out.println("Got scrub_geo event userId:" + userId +
                // "upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                // System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        twitterStream.addListener(listener);

        FilterQuery query = new FilterQuery().track(keyWords);
        twitterStream.filter(query);


    }
}


