import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.sql.SparkSession;

import java.util.*;


public class Main {

    public static void main(String[] args) {

        // To initialize MaliciousDataset which is invoked on static initializer
        KafkaProducerStream kafka1=new KafkaProducerStream();
        kafka1.cheat("1a5Vz1OZyFLk2E1RsyVxEM0Qq","r7Y6rEYsNnxbBbEKym7rmPkhe3C7OkFy8YrtGLqQ81isV0iI2L","1008632466045833216-xcZ86GkP08pz55Rq4eGDBY9MORy5iJ",
                "IjTCiDrM8Fw6bUtOn8TvWXwzvKbkrWvQks7FWV95NWbRD","test","bir,bu,da,o,daha,çok,ne,her,olan,dedi");
        //kafka1.initialize();
        kafka1.setup();
        kafka1.run();

        KafkaProducerStream kafka2=new KafkaProducerStream();
        kafka2.cheat("n91YqdYoHoCjqIg2CI9JtdrTM", "Si7GhqrXUwCYOwQsArG0s9Z2TFmd94E6wFWkhzJQwD0LGsdhbn", "1008632466045833216-aC6ADMKDU2HemjIqcLDDbNDmihtOPh"
                ,"Wd61jiDehXWTUqIuXhB4QCp1RqemFCRDdHujJOkbq18WX","test2","ve,de,için,gibi,ama,sonra,kadar,olarak,diye,ki");
        //kafka2.initialize();
        kafka2.setup();
        kafka2.run();

        Scanner sc = new Scanner(System.in);
        //System.out.println("Please Enter Kafka Topic to Output");
        //String topic=sc.nextLine();



        SparkProcess spark = new SparkProcess();
        spark.setInputOutput("test","test2","Output2");
        spark.setContext();
        spark.stream();
        spark.cacheToTopic();
        spark.runStream();
    }



}
