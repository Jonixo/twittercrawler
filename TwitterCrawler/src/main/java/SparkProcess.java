
import java.util.Properties;

import kafka.Kafka;
import kafka.serializer.StringDecoder;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import org.apache.spark.api.java.JavaSparkContext;
import java.util.*;


public class SparkProcess {
    private Map<String, String> kafkaParams = new HashMap<String, String>();
    private transient JavaSparkContext jsc;
    private JavaStreamingContext jssc;
    private JavaPairInputDStream<String, String> stream = null;
    private SparkConf sconf;
    private Set<String> topicsSet;
    private String outputTopic;
    private HashMap<String, String> hashMap =new HashMap<>();
    private JavaPairRDD<String,String> pairRdd=null;

    public void setInputOutput(String inputTopic1,String inputTopic2, String outputTopic){
        topicsSet = new HashSet<>(Arrays.asList(inputTopic1,inputTopic2));
        this.outputTopic=outputTopic;
    }

    public JavaStreamingContext setContext() {
        sconf = new SparkConf().setMaster("local[2]").setAppName("Test");
        sconf.set("spark.streaming.receiver.writeAheadLog.enable", "true");
        jsc = new JavaSparkContext(sconf);
        jssc = new JavaStreamingContext(jsc, Durations.seconds(1));

        kafkaParams.put("metadata.broker.list", "localhost:9092");

        return jssc;
    }

    public void stream() {
        stream = KafkaUtils.createDirectStream(
                jssc,
                String.class,
                String.class,
                StringDecoder.class,
                StringDecoder.class,
                kafkaParams,
                topicsSet

        );

    }

    public void cacheToTopic(){
        //had to declare Properties here and use it inside "run", making this into a function causes serializable error

        Timer t = new Timer();
        t.scheduleAtFixedRate(
                new TimerTask()
                {
                    public void run()
                    {
                        pairRdd.distinct(); // Dealing with duplication inside main rdd
                        pairRdd.foreach(record -> {

                            //Had to create producer here to fix some weird problems. It looks logs horrible but what can i do, sometimes.


                            //Sending data to "output" topic in kafka
                            KafkaWrapper.send("elasticOutput", record._2);

                        });
                        pairRdd=null;
                    }
                },
                5 * 1000,      // run first occurrence after 1 min
                5 * 1000);  // run every 1 min
    }



    public void runStream() {


        stream.foreachRDD(rdd -> {

            if(pairRdd==null) //Using union on the coming rdd to bypass null exception
                pairRdd=rdd.union(rdd);
            else
                pairRdd=pairRdd.union(rdd);

            pairRdd.cache(); //Caching the data to process later
            pairRdd.count(); //Calling count function because cache() is a lazy function


        });


        jssc.start();



        try {
            jssc.awaitTermination();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }






}
